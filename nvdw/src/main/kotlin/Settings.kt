import com.novisso.cfgFileName
import com.novisso.model.NvSettings
import com.novisso.model.NvSettings.Companion.CONFIG_FILE_NAME
import com.novisso.model.NvSettings.Companion.DESKTOP_GNOME
import com.novisso.model.NvSettings.Companion.configDir

object Settings {
    private var s: NvSettings? = null

    var switchTime: Int
        set(value) { load(); s?.switchTime = value }
        get() {  load(); return s?.switchTime?:60 }

    var checkTime: Int
        set(value) { load(); s?.checkTime = value }
        get() {  load(); return s?.checkTime?:300 }

    var ipcTimeMs: Long
        set(value) { load(); s?.ipcTimeMs = value }
        get() {  load(); return s?.ipcTimeMs?:1000L }
    var dataDir: String
        set(value) { load(); s?.dataDir = value }
        get() {  load(); return s?.dataDir?:"data" }
    var desktop: String
        set(value) { load(); s?.desktop = value }
        get() {  load(); return s?.desktop?: DESKTOP_GNOME }
    var customCmd: String
        set(value) { load(); s?.customCmd = value }
        get() {  load(); return s?.customCmd?:"" }
    var allPos: Double
        set(value) { load(); s?.allPos = value }
        get() {  load(); return s?.allPos?:0.0 }
    var thumbSize: Int
        set(value) { load(); s?.thumbSize = value }
        get() {  load(); return s?.thumbSize?:120 }
    var screenWidth: Double
        set(value) { load(); s?.screenWidth = value }
        get() {  load(); return s?.screenWidth?:960.0 }
    var screenHeight: Double
        set(value) { load(); s?.screenHeight = value }
        get() {  load(); return s?.screenHeight?:800.0 }

    fun save() {
        load()
        s?.save(cfgFileName)
    }

    fun dataPathResolved(base: String): String {
        load()
        return s?.dataPathResolved(base)?:"data"
    }
    
    private fun load() {
        if(s == null) s = NvSettings.load("$configDir/$CONFIG_FILE_NAME")
    }
}