package com.novisso

import javafx.application.Application
import javafx.application.Platform
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.runBlocking
import java.util.*
import kotlin.concurrent.timerTask
import kotlin.coroutines.CoroutineContext

var mainView: MainView? = null

class NvdwApp : Application(), CoroutineScope {
    private var stage: Stage? = null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx

    init {
        mainView = MainView()
    }

    override fun start(pass_stage: Stage?) {
        stage = pass_stage
        stage?.title = "Novisso Desktop Wallpaper"
        val w = Settings.screenWidth
        val h = Settings.screenHeight
        val root = mainView
        val scene = Scene(root, w, h)
        val cssLoc = this.javaClass.getResource("/application.css")
        scene.stylesheets.add(cssLoc.toExternalForm())
        stage?.icons?.add(Image(NvdwApp::class.java.getResourceAsStream("/NvFav_tp.png")))
        stage?.scene = scene
        Timer().schedule(timerTask {
            Platform.runLater { stage?.show() }
        }, 100)
    }

    override fun stop() {
        Settings.screenHeight = stage?.scene?.height?:800.0
        Settings.screenWidth = stage?.scene?.width?:960.0
        Settings.save()
        super.stop()
    }
}

fun main(args: Array<String>) {
    System.setProperty("javafx.preloader", "com.novisso.SplashPreloader" )
    System.setProperty("awt.useSystemAAFontSettings", "lcd")
    System.setProperty("swing.aatext", "true")
    Application.launch(NvdwApp::class.java, *args)
}