package com.novisso

import com.novisso.model.*
import com.novisso.model.ImgPackDB.Companion.IPDB_FILE_NAME
import com.novisso.model.ImgPackDBIdx.Companion.IDX_FILE_NAME
import com.novisso.model.NvIpc.Companion.IPC_FILE_NAME
import com.novisso.model.NvSettings.Companion.CONFIG_FILE_NAME
import com.novisso.model.NvSettings.Companion.configDir
import com.novisso.view.AllTab
import com.novisso.view.PackTab
import com.novisso.view.PrefsTab
import com.novisso.view.TodayTab
import javafx.application.Platform
import javafx.scene.control.TabPane
import kotlinx.coroutines.*
import kotlinx.coroutines.javafx.JavaFx
import java.io.File
import kotlin.coroutines.CoroutineContext

val cfgFileName = "$configDir/$CONFIG_FILE_NAME"
val ipcFileName = "$configDir/$IPC_FILE_NAME"
const val dbFName = IPDB_FILE_NAME
const val idxFName = IDX_FILE_NAME

class MainView: TabPane(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx

    val ipdbMgr = ImgPackDBMgr(dataPath)

    private val todayTab: TodayTab
    private val allTab: AllTab
    private val packTab: PackTab
    private val prefsTab: PrefsTab

    init {
        mkdirs()

        todayTab = TodayTab(ipdbMgr)
        tabs.add(todayTab)

        allTab = AllTab(ipdbMgr)
        tabs.add(allTab)

        packTab = PackTab(ipdbMgr)
        tabs.add(packTab)

        prefsTab = PrefsTab()
        tabs.add(prefsTab)
        stylesheets.add("style.css")
    }

    private fun mkdirs() {
        File(dataPath).mkdirs()
        File("$dataPath/$WALL_DIR").mkdirs()
        File("$dataPath/$IMG_DIR").mkdirs()
        File("$dataPath/$THUMB_DIR").mkdirs()
    }

    fun invalidate() {
        //println("start invalidate")
        launch {
            todayTab.forceRefresh()
            yield()
            packTab.forceRefresh()
            yield()
            allTab.forceRefresh()
        }
        //println("all refresh")
    }

    fun saveDB() = ipdbMgr.saveAll()

    fun imageName(id: Int?) : String = ipdbMgr.imgPackDB.findImgByID(id)?.name?:""
    fun thumbFilename(img: Img) = "$dataPath/$THUMB_DIR/${img.id}${Img.EXT}"
    fun imageFilename(img: Img) = "$dataPath/$IMG_DIR/${img.id}${Img.EXT}"

    val dataPath: String
        get() = Settings.dataPathResolved(configDir)
    val imagePath
        get() = "$dataPath/$IMG_DIR"
    val thumbPath
        get() = "$dataPath/$THUMB_DIR"
    val dbFilename
        get() = "$dataPath/$dbFName"
}
