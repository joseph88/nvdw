package com.novisso

import com.novisso.model.ImgPackDB
import com.novisso.model.ImgPackDBIdx
import com.novisso.modelrw.ImgPackDBRW
import javafx.application.Application.launch
import javafx.application.Platform
import kotlinx.coroutines.*
import java.io.File
import java.lang.Exception
import java.lang.RuntimeException

class ImgPackDBMgr(private val dataPath: String) {
    enum class Status { START, LOADING_IDX, LOADING_DB, LOADED, IDLE, SAVING, SAVED, CLOSING }
    var status = Status.START
        private set(v) {
            field = v
            notify(status)
        }

    val imgPackDBIdx: ImgPackDBIdx
        get() = ipdbIdx?: ImgPackDBIdx()

    val imgPackDB: ImgPackDBRW
        get() = ipDB?: ImgPackDBRW()

    private var ipDB: ImgPackDBRW? = null
    private var ipdbIdx: ImgPackDBIdx? = null
    private val tgts = mutableSetOf<IPDBMgrNotifyTarget>()
    private var needsSave = false

    init {
        loadDB()
        startSaveDaemon()
    }

    fun saveAll() { needsSave = true }

    private fun startSaveDaemon() {
        GlobalScope.launch(Dispatchers.IO) {
            while (true) {
                if (needsSave) {
                    status = Status.SAVING
                    needsSave = false
                    ipdbIdx?.save("$dataPath/$idxFName")
                    yield()
                    ipDB?.save("$dataPath/$dbFName")
                    ipDB?.save2("$dataPath/$dbFName.2")
                    yield()
                    status = Status.SAVED
                    status = Status.IDLE
                } else delay(100)
            }
        }
    }

    fun notifyMe(target: IPDBMgrNotifyTarget) {
        tgts.add(target)
    }

    private fun notify(status: Status) {
        tgts.forEach {
            Platform.runLater {
                it.updateStatus(status)
            }
        }
    }

    private fun reindex() {
        val lst = ipdbIdx?.days?:throw RuntimeException("the ipdbIdx was null")
        ipdbIdx?.days?.forEach { set -> set.clear() }
        ipDB?.allImages()?.forEach {img ->
            img.dates.forEach { doy ->
                lst[doy].add(img.id)
            }
        }
    }

    private fun loadDB() {
        GlobalScope.launch {
            status = Status.LOADING_IDX
            val doReindex = !File("$dataPath/$idxFName").exists()
            ipdbIdx = ImgPackDBIdx.load("$dataPath/$idxFName")
            status = Status.LOADING_DB
            ipDB = try {
                ImgPackDBRW.load("$dataPath/$dbFName")
            } catch (ex: Exception) {
                ex.printStackTrace()
                ImgPackDBRW()
            }
            ipDB?.idx = ipdbIdx
            if(doReindex) {
                reindex()
                ipdbIdx?.save("$dataPath/$idxFName")
            }
            status = Status.LOADED
            status = Status.IDLE
        }
    }
}

interface IPDBMgrNotifyTarget {
    fun updateStatus(status: ImgPackDBMgr.Status)
}