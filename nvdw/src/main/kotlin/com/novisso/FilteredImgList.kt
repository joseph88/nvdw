package com.novisso

import com.novisso.model.Img
import com.novisso.model.ImgPack
import java.util.*

class FilteredImgList(var name: String) {

    var list: MutableList<Img> = mutableListOf()
    set(value) {
        field = value
        sortByDoyIndex()
        invalidate()
    }
    private val listeners: MutableList<ImgListChangeListener> = mutableListOf()
    private var cpos = -1

    private fun sortByDoyIndex() {
        Collections.sort(list, Comparator.comparingInt<Img>(Img::doyIndex))
    }

    operator fun contains(img: Img): Boolean {
        return list.contains(img)
    }

    fun add(img: Img) {
        if (!list.contains(img)) list.add(img)
        cpos = -1
        fireListChanged()
    }

    fun remove(img: Img) {
        list.remove(img)
        cpos = -1
        fireListChanged()
    }

    fun clear() {
        list.clear()
        cpos = -1
        fireListChanged()
    }

    fun addAll(lst: List<Img>) : FilteredImgList {
        list.addAll(lst)
        sortByDoyIndex()
        fireListChanged()
        return this
    }

    fun removePack(pack: ImgPack) {
        if (pack.uuid.toString() == name) list.clear()
        else pack.imgs.forEach { list.remove(it) }
        fireListChanged()
    }

    fun update(img: Img) {
        if (img.isDoyIndexDirty) fireListChanged()
        else fireImgChanged(img)
    }

    private fun invalidate() {
        cpos = -1
        fireListChanged()
    }

    private fun fireListChanged() {
        sortByDoyIndex()
        listeners.forEach { it.imgListChanged() }
    }

    private fun fireImgChanged(img: Img) {
        listeners.forEach { it.imgChanged(img) }
    }

    fun addImgListChangeListener(listener: ImgListChangeListener) {
        listeners.add(listener)
    }

    fun removeImgListChangeListener(listener: ImgListChangeListener) {
        listeners.remove(listener)
    }

    interface ImgListChangeListener {
        fun imgListChanged()
        fun imgChanged(img: Img)
    }

    val idStringList: List<String>
    get() {
        val l = ArrayList<String>(list.size)
        list.forEach { l.add(it.uuid) }
        return l
    }

    fun getImgById(id: String): Img? {
        list.forEach { if (it.uuid == id) return it }
        return null
    }

    val andIncCurr: Img?
    get() {
        if (list.size == 0) return null
        cpos++
        if (cpos >= list.size) cpos = 0
        return list[cpos]
    }
}