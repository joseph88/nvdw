package com.novisso.model

import com.beust.klaxon.Json
import com.beust.klaxon.JsonReader
import com.beust.klaxon.Klaxon

import java.io.File
import java.io.FileReader
import java.lang.StringBuilder

open class ImgPackDB (
    var packs: MutableList<ImgPack> = mutableListOf(),
    protected var nextId: Int = 1
) {
    val fileVer = 1
    @Json(ignored = true)
    var idx: ImgPackDBIdx? = null

    fun dayImages(doy: Int) : List<Img> {
        val imgs = mutableListOf<Img>()
        packs.forEach { pk ->
            pk.imgs.forEach { if(it.isOnDoy(doy)) imgs.add(it) }
        }
        return imgs.toList()
    }

    fun allImages() : List <Img> {
        val imgs = mutableListOf<Img>()
        packs.forEach { pk ->
            pk.imgs.forEach { imgs.add(it) }
        }
        return imgs.toList()
    }

    fun findImgByUUID(id: String?) : Img? {
        if(id != null) {
            packs.forEach {
                val img = it.imgs.find { i -> i.uuid == id }
                if (img != null) return img
            }
        }
        return null
    }

    fun findImgByID(id: Int?) : Img? {
        if(id != null) {
            packs.forEach {
                val img = it.imgs.find { i -> i.id == id }
                if (img != null) return img
            }
        }
        return null
    }

    fun getNextAvailableID(): Int = nextId++

    protected fun loadFill(fileName: String) : ImgPackDB {
        File(fileName).reader().use { inp ->
            //magic number
            if(inp.readInt() != MAGIC_NUMBER) throw Exception("This is not an NVDW file")
            //file version
            val fv = inp.readShort()
            if(fv < 0) throw Exception("Bad file version $fv")
            else if(fv > fileVer) throw Exception("Cannot read version $fv from version $fileVer")
            //next ID
            nextId = inp.readInt()
            if(nextId < 0) throw Exception("Invalid nextId $nextId")
            //pack count
            val cnt = inp.readShort()
            if(cnt < 0) throw Exception("Invalid Pack Count $cnt")
            //packs
            packs.clear()
            for(x in 1..cnt){
                packs.add(ImgPack.read(inp))
            }
        }
        return this
    }

    fun findPackByUUID(uuid: String?) : ImgPack? = packs.find { it.uuid == uuid }

    fun debugString(): String {
        val sb = StringBuilder("Image Pack \n==============================================\n")
        sb.append("Next ID: ").append(nextId)
        packs.forEach { sb.append(it.toString()); sb.append("\n---\n") }
        return sb.toString()
    }

    companion object {
        const val IPDB_FILE_NAME = "db.bin"
        const val MAGIC_NUMBER = 0x4E564457

        fun oldLoad(fileName: String) : ImgPackDB {
            val f = File(fileName)
            return if(f.exists()) Klaxon().parse<ImgPackDB>(f)?:ImgPackDB() else ImgPackDB()
        }

        fun load(fileName: String) : ImgPackDB = ImgPackDB().loadFill(fileName)

        fun loadTodayImgs(fileName: String) : List<Img> {
            val tod = getTodayDoy()
            val list = mutableListOf<Img>()
            JsonReader(FileReader(File(fileName))).use { r ->
                r.beginObject {
                    val pks = r.nextName()
                    //println("read $pks")
                    if (pks == "packs") {
                        //println("found packs")
                        r.beginArray {
                            while (r.hasNext()) {
                                //println("found pack in array")
                                r.beginObject {
                                    while(r.hasNext()) {
                                        val nn = r.nextName()
                                        //println("read $nn")
                                        if (nn == "imgs") {
                                            r.beginArray {
                                                while (r.hasNext()) {
                                                    //println("found img in array")
                                                    val img = Klaxon().parse<Img>(r)
                                                    if (img != null && img.isOnDoy(tod)) {
                                                        //println("adding image ${img.name}")
                                                        list.add(img)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return list
        }
    }
}
