package com.novisso.model

import com.beust.klaxon.Klaxon
import java.io.File

/**
 *   switchTime: specified in seconds
 *   checckTime: specified in seconds
 *   ipcTimeMS: specified in millis (so 1000L is every second)
 *   dataDir: if this starts with a '/' its absolute if not its relative to the base dir (/home/user/.config/nvdw)
 *   desktop: Which desktop to switch for [gnome, custom]
 *   customCmd: a shell executable command to switch the wallpaper
 *   allPos: the position to scroll to for all images
 *   thumbSize: the height of thumbnails to scale to in pixels (keeping aspect) in the editor
 *   screenWidth: saves the last screen width of the editor
 *   screenHeight saves the last screen height of the editor
 */

class NvSettings (
    var switchTime: Int = 60,
    var checkTime: Int = 300,
    var ipcTimeMs: Long = 1000L,
    var dataDir: String = "data",
    var desktop: String = DESKTOP_GNOME,
    var customCmd: String = "",
    var allPos: Double = 0.0,
    var thumbSize: Int = 120,
    var screenWidth: Double = 960.0,
    var screenHeight: Double = 800.0
) {
    fun dataPathResolved(base: String) : String = if(dataDir.startsWith('/')) dataDir else "$base/$dataDir"

    fun save(fileName: String) {
        File(fileName).printWriter().use { out ->
            out.print(Klaxon().toJsonString(this))
            out.println()
        }
    }

    companion object {
        const val CONFIG_FILE_NAME = "config.json"
        val configDir = "${System.getProperty("user.home")}/.config/nvdw"
        fun load(fileName: String) : NvSettings {
            val f = File(fileName)
            return if(f.exists()) Klaxon().parse<NvSettings>(f)?: NvSettings() else NvSettings()
        }

        const val DESKTOP_GNOME = "gnome"
        const val DESKTOP_KDE = "kde"
        const val DESKTOP_CUSTOM = "custom"
    }
}
