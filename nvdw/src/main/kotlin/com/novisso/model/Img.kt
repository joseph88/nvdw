package com.novisso.model

import com.beust.klaxon.Json
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.util.*
import kotlin.Exception

class Img (
    var name: String = "",
    var origFile: String = "",
    var id: Int = 0,
    var dates: MutableList<Int> = mutableListOf(),
    private var rate: Int = 2,
    var uuid: String = "${UUID.randomUUID()}"
) {
    @Json(ignored = true)
    var isDoyIndexDirty = false
        private set

    fun isOnDoy(doy: Int) = dates.contains(doy)
    fun doyIndex(): Int = if (dates.size > 0) dates[0] else -1

    fun toggleOnDay(doy: Int): Boolean {
        dates.forEach {
            if(it == doy) {
                dates.remove(it)
                return false
            }
        }
        dates.add(doy)
        dates.sort()
        return true
    }

    override fun toString(): String {
        return "$name : $origFile : $dates : $rate : $id : $uuid"
    }

    internal fun write(out: OutputStreamWriter) {
        out.writeInt(id)
        out.writeByte(rate.toByte())
        out.writeString(uuid)
        out.writeString(name)
        out.writeString(origFile)
        out.writeShort(dates.size)
        dates.forEach { date ->
            out.writeShort(date)
        }
    }

    internal fun write2(out: OutputStreamWriter) {
        out.write("$id|")
        out.write("$rate|")
        out.write("$uuid|")
        out.write("$name|")
        out.write("$origFile|")
        out.write("${dates.size}|")
        dates.forEach { date ->
            out.write("$date|")
        }
    }

    private fun readFill(inp: InputStreamReader) : Img {
        id = inp.readInt()
        if(id < 1) throw Exception("Invalid Img ID: $id")
        this.rate = inp.readByte().toInt()
        if(rate < 0) throw Exception("Invalid rating: $rate")
        val u = inp.readString()
        if(uuid.length != u.length) throw Exception("Invalid UUID $u")
        uuid = u
        name = inp.readString()
        origFile = inp.readString()
        val cnt = inp.readShort()
        if(cnt < 0) throw Exception("Invalid Date Count $cnt")
        var d: Int
        for(x in 1..cnt) {
            d = inp.readShort()
            if(d < 0 || d > 366) throw Exception("Invalid date $d")
            dates.add(d)
        }
        return this
    }

    companion object {
        const val EXT = ".jpg"

        fun read(inp: InputStreamReader) : Img {
            return Img().readFill(inp)
        }
    }
}