package com.novisso

import com.beust.klaxon.Klaxon
import com.novisso.model.IMG_DIR
import com.novisso.model.Img
import com.novisso.model.Img.Companion.EXT
import com.novisso.model.ImgPack
import com.novisso.model.THUMB_DIR
import javafx.application.Platform
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.awt.RenderingHints
import java.awt.image.BufferedImage
import java.io.*
import java.nio.channels.FileChannel
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import javax.imageio.IIOImage
import javax.imageio.ImageIO
import javax.imageio.ImageWriteParam
import javax.imageio.ImageWriter
import javax.imageio.stream.FileImageOutputStream
import java.util.zip.ZipFile

const val THUMB_QUALITY = 0.88f
const val IMG_QUALITY = 0.94f
const val THUMB_SCALE_MAX = 280
const val BUFFER_SIZE = 8192

@Throws(IOException::class)
suspend fun scaleAndSaveImageWithJImage(sourceImage: BufferedImage, destFile: String, width: Int, height: Int, quality: Float) {
    withContext(Dispatchers.IO) {
        val dbi = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
        val g = dbi.createGraphics()
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC)
        g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY)
        g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY)
        g.drawImage(sourceImage, 0, 0, width, height, null)
        writeJpeg(dbi, destFile, quality)
    }
}

@Throws(IOException::class)
suspend fun writeJpeg(image: BufferedImage, destFile: String, quality: Float) {
    withContext(Dispatchers.IO) {
        var writer: ImageWriter? = null
        var output: FileImageOutputStream? = null
        try {
            writer = ImageIO.getImageWritersByFormatName("jpeg").next()
            val param = writer.defaultWriteParam
            param.compressionMode = ImageWriteParam.MODE_EXPLICIT
            param.compressionQuality = quality
            output = FileImageOutputStream(File(destFile))
            writer.output = output
            val iioImage = IIOImage(image, null, null)
            writer.write(null, iioImage, param)
        } catch (ex: IOException) {
            throw ex
        } finally {
            writer?.dispose()
            output?.close()
        }
    }
}

suspend fun exportPack(pack: ImgPack, filename: String) {
    println("export Start")
    withContext(Dispatchers.IO) {
        val f = File(filename)
        val out = ZipOutputStream(FileOutputStream(f))
        val e = ZipEntry("pack.json")
        out.putNextEntry(e)

        val writer = out.writer()
        val str = Klaxon().toJsonString(pack)
        writer.write(str)
        writer.flush()
        out.closeEntry()

        out.putNextEntry(ZipEntry("imgs/"))
        out.closeEntry()

        //byte[] data = sb.toString().getBytes()
        //out.write(data, 0, data.length)
        mainView?.let { mv ->
            pack.imgs.forEach {
                val ifn = mv.imageFilename(it)
                val file = File(ifn)
                if (file.exists()) {
                    val istr = FileInputStream(file)
                    val ientry = ZipEntry("imgs/${it.uuid}.jpg")
                    out.putNextEntry(ientry)
                    istr.copyTo(out)
                    istr.close()
                    out.closeEntry()
                } else {
                    println("missing file $ifn")
                }
            }
        }

        out.closeEntry()
        out.close()
    }
    println("export complete")
}

suspend fun importPack(filename: String) {
    withContext(Dispatchers.IO) {
        val zipFile = ZipFile(filename)
        var pack: ImgPack? = null
        zipFile.entries().iterator().forEach fnd@{
            if (!it.isDirectory && it.name == "pack.json") {
                val pin = zipFile.getInputStream(it)
                pack = Klaxon().parse<ImgPack>(pin)
                return@fnd
            }
        }

        val dirPath = "${Settings.dataDir}${File.separator}temp"
        File("$dirPath${File.separator}$IMG_DIR").mkdirs()

        zipFile.entries().iterator().forEach {
            if (!it.isDirectory) {
                //println("found File ${it.name}:${it.size}")
                extractFile(zipFile.getInputStream(it), "$dirPath${File.separator}${it.name}")
            }
        }

        //nuke the previous pack
        val nukePack = mainView?.ipdbMgr?.imgPackDB?.findPackByUUID(pack?.uuid)
        nukePack?.let {
            mainView?.ipdbMgr?.imgPackDB?.removePack(
                it,
                "${Settings.dataDir}${File.separator}$IMG_DIR",
                "${Settings.dataDir}${File.separator}$THUMB_DIR"
            )
        }

        val imgList = pack?.imgs?.toList()
        pack?.imgs?.clear()
        imgList?.forEach {
            val srcFile = "$dirPath${File.separator}$IMG_DIR${File.separator}${it.uuid}$EXT"
            val i = pack?.createNewImage(srcFile, mainView?.ipdbMgr?.imgPackDB?.getNextAvailableID() ?: 0)
            i?.let { vi ->
                vi.dates = it.dates
                vi.name = it.name
                vi.origFile = srcFile
                vi.uuid = it.uuid
                //println("copying Image: ${vi.name} :: ${vi.origFile}")
                mainView?.let { mv -> copyImageFile(vi, mv) }
                vi.origFile = it.origFile
                File(srcFile).delete()
                //println("image copied and original deleted: ${vi.name}")
            }
        }

        pack?.let { mainView?.ipdbMgr?.imgPackDB?.packs?.add(it) }
        mainView?.saveDB()
    }
    println("Import Done")
}

suspend fun copyImageFile(img: Img, mv: MainView) {
    try {
        withContext(Dispatchers.IO) {
            try {
                val sbi = ImageIO.read(File(img.origFile))
                var h = sbi.height
                var w = sbi.width
                val ratio = h.toFloat() / w.toFloat()
                if (w > 4096) {
                    w = 4096
                    h = (4096 * ratio).toInt()
                }
                if (h > 2160) {
                    h = 2160
                    w = (2160 / ratio).toInt()
                }

                scaleAndSaveImageWithJImage(sbi, mv.imageFilename(img), w, h, IMG_QUALITY)
                scaleAndSaveImageWithJImage(
                    sbi,
                    mv.thumbFilename(img),
                    THUMB_SCALE_MAX,
                    (THUMB_SCALE_MAX * ratio).toInt(),
                    THUMB_QUALITY
                )

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    } catch (ie: InterruptedException) {
        println(ie.message)
    }
}

@Throws(IOException::class)
private suspend fun extractFile(zipIn: InputStream, filePath: String) {
    withContext(Dispatchers.IO) {
        val bos = BufferedOutputStream(FileOutputStream(filePath))
        val bytesIn = ByteArray(BUFFER_SIZE)
        var read = zipIn.read(bytesIn)
        while (read != -1) {
            bos.write(bytesIn, 0, read)
            read = zipIn.read(bytesIn)
        }
        bos.close()
    }
}