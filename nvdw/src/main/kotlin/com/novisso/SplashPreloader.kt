package com.novisso

import javafx.application.Platform
import javafx.application.Preloader
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.Pane
import javafx.stage.Stage
import javafx.stage.StageStyle
import java.util.*
import kotlin.concurrent.timerTask

class SplashPreloader : Preloader() {
    var stage: Stage? = null
    override fun start(stage: Stage?) {
        this.stage = stage
        stage?.icons?.add(Image(SplashPreloader::class.java.getResourceAsStream("/NvFav_tp.png")))
        stage?.initStyle(StageStyle.UNDECORATED)
        stage?.isAlwaysOnTop = true
        val img = Image(SplashPreloader::class.java.getResourceAsStream("/nvdwm.png"))
        img.progress
        val iv = ImageView(img)
        val pane = Pane()
        pane.children.add(iv)
        stage?.scene = Scene(pane)

        Timer().schedule(timerTask {
            Platform.runLater{ stage?.show() }
        }, 100)
    }

    override fun handleStateChangeNotification(info: StateChangeNotification?) {
        super.handleStateChangeNotification(info)
        if(info?.type?.name == "BEFORE_START"){
            //println("scheduling stop...")
            Timer().schedule(timerTask {
                Platform.runLater { stage?.close() }
            }, 5000)
        }
    }

//    override fun handleApplicationNotification(info: PreloaderNotification?) {
//        super.handleApplicationNotification(info)
//        Timer().schedule(timerTask {
//            Platform.runLater { stage?.close() }
//            println("app kill")
//        }, 4000)
//    }
}