package com.novisso.view

import javafx.scene.control.*

/**
 * Created by joseph on 1/9/17.
 */
class CreatePackDialog : Dialog<ButtonType>() {
    init {
        val dp = dialogPane
        dp.buttonTypes.add(ButtonType("OK", ButtonBar.ButtonData.OK_DONE))
    }
}
