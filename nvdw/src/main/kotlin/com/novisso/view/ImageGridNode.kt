package com.novisso.view

import com.novisso.*
import com.novisso.model.Img
import com.novisso.view.calendar.DayNode
import com.novisso.view.calendar.MonthNode
import javafx.application.Platform
import javafx.geometry.*
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import java.io.File
import java.io.IOException
import javax.imageio.ImageIO

import kotlinx.coroutines.*
import kotlinx.coroutines.javafx.JavaFx
import kotlin.coroutines.CoroutineContext


/**
 * Created by joseph on 2/22/17.
 */
class ImageGridNode(val img: Img) : VBox(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx
    private val iv: ImageView = ImageView()
    private val title: Label = Label()
    private val info: Label = Label()
    private var drawn = false
    private var nodeH = Settings.thumbSize.toDouble()
    private var nodeW = Settings.thumbSize.toDouble()

    init {
        val sp = StackPane()
        sp.setPrefSize(nodeW, (nodeH - INFO_H))
        sp.setMaxSize(nodeW, (nodeH - INFO_H))
        sp.setMinSize(nodeW, (nodeH - INFO_H))
        sp.alignment = Pos.CENTER
        sp.children.add(iv)
        sp.style = "-fx-background-color: #222222;"

        title.style = "-fx-font-size: 11pt;" +
                "-fx-text-fill: #76b2f7;"

        info.style = "-fx-font-size: 9.5pt;" +
                "-fx-text-fill: #76b2f7;"

        setFromImg()
        setPrefSize(nodeW, nodeH)
        this.children.addAll(sp, title, info)
        this.setOnMouseClicked {
            val ied = ImgEditorDialog(img)
            ied.showAndWait()
        }

        style = "-fx-background-color: #000000;"
    }

    fun drawImg() {
        launch {
            if(!drawn) {
                drawn = true
                try {
                    mainView?.let { mv ->
                        if (!File(mv.thumbFilename(img)).exists()) missingThumb()
                        iv.image =
                            Image(
                                "file:" + mv.thumbFilename(img), nodeW, (nodeH - INFO_H),
                                true, true, true
                            )
                    }
                } catch (ex: Exception) { }
            }
        }
    }

    private fun setFromImg() {
        title.text = img.name
        info.text = getDateDisp(img.doyIndex())
        drawn = false
    }

    private suspend fun missingThumb() {
        withContext(Dispatchers.IO) {
            try {
                mainView?.let { mv ->
                    val sbi = ImageIO.read(File(mv.imageFilename(img)))
                    val ratio = sbi.height.toFloat() / sbi.width.toFloat()
                    scaleAndSaveImageWithJImage(
                        sbi, mv.thumbFilename(img),
                        THUMB_SCALE_MAX, (THUMB_SCALE_MAX * ratio).toInt(),
                        THUMB_QUALITY
                    )
                }
            } catch (e: IOException) {
                println("loading thumb: ${mainView?.imageFilename(img)} for ${img.name}")
                e.printStackTrace()
            }
        }
    }

    fun refresh(img: Img): Boolean {
        if (this.img != img) return false
        if(this.isVisible)  setFromImg()
        return true
    }

    companion object {
        private const val INFO_H = 36.0

        fun getDateDisp(doy: Int): String {
            if (doy < 0) {
                return "(unset)"
            }
            if (doy > 365) {
                return "(error)"
            }
            return "${MonthNode.MonthNames[DayNode.getMonthFromDoy(doy)]} ${(DayNode.getMonthDayFromDoy(doy) + 1)}"
        }
    }
}
