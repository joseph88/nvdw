package com.novisso.view

import com.novisso.FilteredImgList
import com.novisso.IPDBMgrNotifyTarget
import com.novisso.ImgPackDBMgr
import com.novisso.model.getTodayDoy
import javafx.animation.Animation
import javafx.animation.KeyFrame
import javafx.animation.KeyValue
import javafx.animation.Timeline
import javafx.util.Duration
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield
import kotlin.coroutines.CoroutineContext

/**
 * Created by joseph on 1/4/17.
 */
class TodayTab(db: ImgPackDBMgr) : ImageTab("Today", db), IPDBMgrNotifyTarget, CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx

    //Timer t = new Timer();
    private var tl: Timeline
    private var todays = FilteredImgList("today")
    private var todayDoy = getTodayDoy()

    init {
        imgpane = ImageGridPane(todays, this)
        content = imgpane
        db.notifyMe(this)
        tl = Timeline(KeyFrame(Duration.minutes(1.0), { refresh() }, emptyArray<KeyValue>()))
        tl.cycleCount = Animation.INDEFINITE
        tl.play()
        if(db.status == ImgPackDBMgr.Status.IDLE){
            todays.addAll(db.imgPackDB.dayImages(getTodayDoy()))
        }
    }

    override fun refresh() {
        //System.out.println("refreshing TodayTab");
        if (todayDoy != getTodayDoy()) {
            todays = FilteredImgList("today").addAll(db.imgPackDB.dayImages(getTodayDoy()))
            todayDoy = getTodayDoy()
        }
    }

    suspend fun forceRefresh() {
        val todayImgs = db.imgPackDB.dayImages(getTodayDoy())
        yield()
        todays = FilteredImgList("today").addAll(todayImgs)
        todayDoy = getTodayDoy()
        imgpane?.setFilist(todays)
    }

    override fun updateStatus(status: ImgPackDBMgr.Status) {
        if(status == ImgPackDBMgr.Status.LOADED) {
            launch { forceRefresh() }
        }
    }
}
