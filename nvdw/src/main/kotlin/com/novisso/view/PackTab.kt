package com.novisso.view

import com.novisso.*
import com.novisso.model.ImgPack
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Insets
import javafx.scene.control.*
import javafx.scene.input.DragEvent
import javafx.scene.input.TransferMode
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.stage.FileChooser
import kotlinx.coroutines.*
import kotlinx.coroutines.javafx.JavaFx

import java.io.File
import kotlin.coroutines.CoroutineContext

/**
 * Created by joseph on 1/4/17.
 */
class PackTab(db: ImgPackDBMgr) : ImageTab("Packs", db), IPDBMgrNotifyTarget, CoroutineScope {
    private var ctrl: FlowPane = FlowPane()
    private var packChoice: ChoiceBox<ImgPack> = ChoiceBox()
    private var addPack: Button = Button("+")
    private var delPack: Button = Button("-")
    private var prog = Label("")
    private var expPack: Button = Button("Export")
    private var impPack: Button = Button("Import")
    private var editPack: Button = Button("Edit")
    private val filMap = mutableMapOf<String, FilteredImgList>()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx

    init {
        db.notifyMe(this)
        ctrl.hgapProperty().value = 6.0
        ctrl.paddingProperty().value = Insets(3.0, 8.0, 0.0, 8.0)

        ctrl.children.add(packChoice)
        ctrl.children.add(addPack)
        ctrl.children.add(delPack)
        ctrl.children.add(editPack)
        ctrl.children.add(expPack)
        ctrl.children.add(impPack)
        ctrl.children.add(prog)

        addPack.setOnAction {
            val tid = TextInputDialog()
            tid.contentText = "New Package Name"
            tid.headerText = "Add a New Image Package"
            tid.title = "New Package"
            tid.showAndWait().ifPresent { result ->
                //val pack = db.createNewPack(result)
                val pack = ImgPack(result)
                db.imgPackDB.packs.add(pack)
                mainView?.let { mv ->
                    GlobalScope.launch {
                        db.imgPackDB.save(mv.dbFilename)
                    }
                }
                //list.add(pack)
                packChoice.items.add(pack)
                packChoice.selectionModel.select(pack)
            }
        }


        delPack.setOnAction { e ->
            val p = currPack
            if (p != null) {
                val alert = Alert(Alert.AlertType.CONFIRMATION)
                alert.title = "Delete Package"
                alert.headerText = "Delete Package: " + p.name
                alert.contentText = "Are your sure you want to delete this package and all its images!?"

                val result = alert.showAndWait()
                if (result.get() == ButtonType.OK) {
                    mainView?.let { mv ->
                        GlobalScope.launch {
                            mv.ipdbMgr.imgPackDB.removePack(p, mv.imagePath, thumbPath = mv.thumbPath)
                            mv.saveDB()
                            Platform.runLater {
                                loadPackChoices()
                                mv.invalidate()
                            }
                        }
                    }
                }
            }
            e.consume()
        }

        if(db.status == ImgPackDBMgr.Status.IDLE) {
            loadPackChoices()
        }

        packChoice.setOnAction {
            refresh()
            launch { imgpane?.drawNodes() }
        }

        editPack.setOnAction { _->
            val p = currPack
            if(p != null) {
                val ied = PackEditorDialog(p)
                ied.showAndWait()
            }
        }

        // EXPORT Button
        expPack.setOnAction { _->
            val p = currPack
            if (p != null) {
                val name = "/home/joseph/${p.name}-nvdw_pack.zip"
                prog.text = "Exporting pack: ${p.name}"
                GlobalScope.launch {
                    exportPack(p, name)
                    Platform.runLater {
                        prog.text = "Export Complete"
                    }
                }
            }
        }

        // IMPORT Button
        impPack.setOnAction { _->
            //val name = "/home/joseph/test-nvdw_pack.zip"
            val flch = FileChooser()
            flch.title = "Select File to Import"
            flch.extensionFilters.add(FileChooser.ExtensionFilter("Zip Files", "*.zip"))
            val file = flch.showOpenDialog(mainView?.scene?.window)
            if(file?.exists() == true) {
                prog.text = "Importing Pack: ${file.nameWithoutExtension}"
                GlobalScope.launch {
                    importPack(file.absolutePath)
                    Platform.runLater {
                        prog.text = "Import Complete"
                        mainView?.invalidate()
                    }
                }
            }
        }

        imgpane = ImageGridPane(this)
        imgpane?.setOnDragOver { event: DragEvent ->
            val dragboard = event.dragboard
            if (dragboard.hasFiles()) event.acceptTransferModes(TransferMode.COPY)
        }
        imgpane?.setOnDragDropped { event: DragEvent ->
            val dragb = event.dragboard
            if (dragb.hasFiles()) {
                val files = dragb.files
                addImages(files)
            }
        }

        val pbp = BorderPane(imgpane, ctrl, null, null, null)
        content = pbp
        refresh()
    }

    override fun updateStatus(status: ImgPackDBMgr.Status) {
        if(status == ImgPackDBMgr.Status.LOADED) {
            loadPackChoices()
        }
    }

    fun loadPackChoices() {
        val suuid = packChoice.selectionModel.selectedItem?.uuid
        packChoice.items.clear()
        packChoice.items.addAll(db.imgPackDB.packs)
        packChoice.items.sortBy { it.name }
        if(suuid != null) {
            val sp = packChoice.items.find { it.uuid == suuid }
            if(sp != null) {
                packChoice.selectionModel.select(sp)
            } else if(packChoice.items.isNotEmpty()) {
                packChoice.selectionModel.selectFirst()
            }
        } else if(packChoice.items.isNotEmpty()) {
            packChoice.selectionModel.selectFirst()
        }
    }

    private fun addImages(files: List<File>){
        val pack = currPack ?: return
        val max = files.size
        prog.text = "Processing $max images"
        var n = 0
        GlobalScope.launch {
            mainView?.let { mv ->
                files.forEach { f ->
                    try {
                        val img = pack.createNewImage(f.absolutePath, db.imgPackDB.getNextAvailableID())
                        copyImageFile(img, mv)
                        n++
                        Platform.runLater { prog.text = "Processed $n of $max images." }
                    } catch (ie: InterruptedException) {
                        println(ie.message)
                    }
                }
                mv.saveDB()
                Platform.runLater {
                    mv.invalidate()
                }
            }
        }
    }

    override fun refresh() {
        loadPackChoices()
        val ip = packChoice.selectionModel.selectedItem
        if (ip != null) {
            if(!filMap.contains(ip.uuid)) filMap[ip.uuid] = FilteredImgList(ip.uuid).addAll(ip.imgs)
            imgpane?.setFilist(filMap[ip.uuid])
        }
    }

    suspend fun forceRefresh() {
        loadPackChoices()
        val ip = packChoice.selectionModel.selectedItem
        if (ip != null) {
            filMap[ip.uuid] = FilteredImgList(ip.uuid).addAll(ip.imgs)
            yield()
            imgpane?.setFilist(filMap[ip.uuid])
        }
    }

    private val currPack: ImgPack?
        get() = packChoice.selectionModel.selectedItem

}

fun MutableList<ImgPack>.observable() : ObservableList<ImgPack> {
    return FXCollections.observableList(this)
}
