package com.novisso.view

import com.novisso.ImgPackDBMgr
import javafx.scene.control.Tab
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

/**
 * Created by joseph on 1/4/17.
 */
open class ImageTab(private var title: String, protected var db: ImgPackDBMgr) : Tab(title), CoroutineScope {
    internal var imgpane: ImageGridPane? = null
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx

    var scrollVValue: Double = 0.0

    init {
        isClosable = false
        selectedProperty().addListener { _->
            launch{ imgpane?.drawNodes() }
        }
    }

    fun setCount(count: Int) { text = "$title ($count)" }
    open fun refresh() {
        captureVScroll()
        imgpane?.refresh()
    }

    internal fun captureVScroll() {
        scrollVValue = imgpane?.vvalueProperty()?.value?:0.0
    }

    open fun onRefreshComplete() {
        imgpane?.vvalueProperty()?.value = scrollVValue
    }
}
