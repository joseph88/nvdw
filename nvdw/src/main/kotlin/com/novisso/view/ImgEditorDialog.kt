package com.novisso.view

import com.novisso.mainView
import com.novisso.model.Img
import com.novisso.view.calendar.CalendarNode
import javafx.event.ActionEvent
import javafx.geometry.Insets
import javafx.scene.control.*

import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.javafx.JavaFx
import kotlin.coroutines.CoroutineContext

/**
 * Created by joseph on 2/22/17.
 */
class ImgEditorDialog(private var img: Img) : Dialog<ButtonType>(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx
    //private var img: Img? = null
    private val name: TextField
    private val calendarNode: CalendarNode

    init {
        //img = editMe
        val fp = BorderPane()
        fp.style = "-fx-background-color: #000000;"

        val i = Image("file:${mainView?.imageFilename(img)}")
        val iv = ImageView(i)
        iv.fitWidth = 500.0
        iv.fitHeight = 320.0
        iv.isPreserveRatio = true

        val ipane = Pane(iv)
        ipane.padding = Insets(4.0)

        //fp.left = ipane

        val id = Label(img.uuid)
        fp.bottom = id

        name = TextField(img.name)
        calendarNode = CalendarNode(img)
        val nuke = Button("delete")
        nuke.setOnAction {
            mainView?.let { mv ->
                mv.ipdbMgr.imgPackDB.removeImage(img, mv.imagePath, mv.thumbPath)
                async { mv.saveDB() }
                val btOk = dialogPane.lookupButton(ButtonType.OK) as Button
                btOk.fire()
            }
        }

        val left = VBox(iv, name, nuke)
        left.padding = Insets(2.0, 8.0, 0.0, 0.0)
        left.spacing = 5.0
        fp.left = left

        val vbox = VBox(calendarNode)

        fp.center = vbox

        dialogPane.content = fp
        dialogPane.setPrefSize(1000.0, 600.0)
        dialogPane.buttonTypes.add(ButtonType.OK)
        dialogPane.buttonTypes.add(ButtonType.CANCEL)

        val btOk = dialogPane.lookupButton(ButtonType.OK) as Button
        btOk.addEventFilter(ActionEvent.ACTION) {
            mainView?.let { mv ->
                img.name = name.text
                mv.saveDB()
                mv.invalidate()
            }
        }
        dialogPane.stylesheets.add("style.css")
    }
}
