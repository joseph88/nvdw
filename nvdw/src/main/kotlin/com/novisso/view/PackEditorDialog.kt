package com.novisso.view

import com.novisso.mainView
import com.novisso.model.ImgPack
import javafx.application.Platform
import javafx.event.ActionEvent
import javafx.geometry.Insets
import javafx.scene.control.*

import javafx.scene.layout.*
import kotlinx.coroutines.*
import kotlinx.coroutines.javafx.JavaFx
import kotlin.coroutines.CoroutineContext

/**
 * Created by joseph on 2/22/17.
 */
class PackEditorDialog(private var pack: ImgPack) : Dialog<ButtonType>(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx

    private val name = TextField()
    private val description = TextArea()

    init {
        val fp = BorderPane()
        fp.style = "-fx-background-color: #000000;"

        val id = Label(pack.uuid)
        fp.bottom = id

        name.textProperty().value = pack.name
        description.textProperty().value = pack.descr

        val left = VBox(name, description)
        left.padding = Insets(2.0, 8.0, 0.0, 0.0)
        left.spacing = 5.0
        fp.left = left

        dialogPane.content = fp
        dialogPane.setPrefSize(1000.0, 600.0)
        dialogPane.buttonTypes.add(ButtonType.OK)
        dialogPane.buttonTypes.add(ButtonType.CANCEL)

        val btOk = dialogPane.lookupButton(ButtonType.OK) as Button
        btOk.addEventFilter(ActionEvent.ACTION) {
            mainView?.let { mv ->
                pack.name = name.textProperty().value
                pack.descr = description.textProperty().value
                async { mv.saveDB() }
                mv.invalidate()
            }
        }
        dialogPane.stylesheets.add("style.css")
    }
}
