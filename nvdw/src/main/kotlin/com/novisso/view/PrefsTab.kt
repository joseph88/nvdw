package com.novisso.view

import com.novisso.model.NvIpc
import com.novisso.ipcFileName
import com.novisso.mainView
import com.novisso.model.NvIpc.Companion.CHK_UPDATE
import com.novisso.model.NvIpc.Companion.LIP_CYCLE_OVERDUE_MILLIS
import com.novisso.model.NvIpc.Companion.NXT_LOAD_NEXT
import com.novisso.model.NvIpc.Companion.PAZ_CONTINUE
import com.novisso.model.NvIpc.Companion.PAZ_PAUSE
import com.novisso.model.NvIpc.Companion.XIT_QUIT
import com.novisso.model.NvIpc.Companion.XIT_RUNNING
import com.novisso.model.NvIpc.Companion.XIT_TERMINATED
import com.novisso.model.NvSettings
import com.novisso.model.now
import javafx.application.Platform
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.*
import javafx.scene.layout.HBox
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.nio.file.Paths
import java.text.SimpleDateFormat
import kotlin.concurrent.timer

class PrefsTab : Tab("Preferences") {
    private val desktopEntry = "${System.getProperty("user.home")}/.config/autostart/nvdwd.desktop"
    private val dataDir= TextField()
    private val swTime= Spinner<Int>(10, 86400, 120)
    private val ckTime= Spinner<Int>(10, 86400, 120)
    private val useGnome = RadioButton("Use Gnome wallpaper switch command")
    private val useKde = RadioButton("Use KDE Plasma wallpaper switch command")
    private val useCustom = RadioButton("Use custom wallpaper switch command")
    private val customSw = TextField()
    private val switched = Label("Last Image Switch")
    private val switchedTime = Label()
    private val chRotate = Label()
    private val currImg = Label("Current Image Name")
    private val currImgName = Label()
    private val pauseBtn= Button()
    private val stopBtn = Button()
    private val thumbSpin = Spinner<Int>(100, 240, 120)
    private val advPane: Pane
    private val advBtn = Button("More")
    private val startupCheck = CheckBox("Launch on Startup")


    private var swTimeDirty: Boolean = false
    private var ckTimeDirty: Boolean = false
    private var desktopDirty: Boolean = false
    private var dataDirty: Boolean = false
    private var customDirty: Boolean = false
    private var thumbDirty: Boolean = false

    init {
        isClosable = false

        val lbl = Insets(10.0, 8.0, 0.0, 0.0)
        val swl = Label("SwitchTime (Seconds):")
        swl.padding = lbl
        val ckl = Label("CheckTime (Seconds):")
        ckl.padding = lbl
        val tml = Label("ThumbSize (Pixels):")
        tml.padding = lbl
        val cswl = Label("Custom Switch Command")
        cswl.padding = lbl
        val ddl = Label("Data Directory:")
        val advVboxL = VBox(swl, swTime, ckl, ckTime, startupCheck, useGnome)
        advVboxL.spacing = 8.0
        val advVboxR = VBox(useCustom, cswl, customSw, ddl, dataDir, tml, thumbSpin)
        advVboxR.spacing = 8.0
        val advSplit = HBox(advVboxL, advVboxR)
        advSplit.spacing = 20.0
        advPane = Pane(advSplit)

        startupCheck.isSelected = File(desktopEntry).exists()

        this.setOnSelectionChanged {
            buildUI()
        }
    }

    private fun buildUI() {
        val ipc = NvIpc.load(ipcFileName)
        //println(ipc)
        mainView?.let { mv ->
            swTime.valueFactory.value = Settings.switchTime
            swTime.isEditable = true
            swTime.valueProperty().addListener { _ -> swTimeDirty = true }

            ckTime.valueFactory.value = Settings.checkTime
            ckTime.isEditable = true
            ckTime.valueProperty().addListener { _ -> ckTimeDirty = true }

            switchedTime.text = tsToDateString(ipc.lsw)
            switchedTime.style = "-fx-font-size: 20pt; -fx-font-weight: bold;"

            switched.padding = Insets(0.0, 8.0, 16.0, 0.0)

            currImgName.text = mv.imageName(ipc.cur)
            currImgName.style = "-fx-font-size: 16pt; -fx-font-weight: bold;"

            currImg.padding = Insets(0.0, 8.0, 16.0, 0.0)

            thumbSpin.valueFactory.value = Settings.thumbSize
            thumbSpin.isEditable = true
            thumbSpin.valueProperty().addListener { _ -> thumbDirty = true }

            chRotate.text = "Last Image Check: ${tsToDateString(ipc.lcr)}"
            pauseBtn.text = if (ipc.paz == PAZ_PAUSE) "Play" else "Pause"

            val statusTextVBox = VBox(switchedTime, switched, currImgName, currImg, chRotate)
            statusTextVBox.minWidth = 380.0
            statusTextVBox.alignment = Pos.TOP_LEFT

            pauseBtn.setOnAction {
                println("paused pressed")
                GlobalScope.launch {
                    val ipc2 = NvIpc.load(ipcFileName)
                    if (ipc2.paz == PAZ_PAUSE) {
                        ipc2.paz = PAZ_CONTINUE
                        Platform.runLater { pauseBtn.text = "Pause" }
                    } else {
                        ipc2.paz = PAZ_PAUSE
                        Platform.runLater { pauseBtn.text = "Play" }
                    }
                    ipc2.save(ipcFileName)
                }
            }

            val nextBtn = Button("Switch!")
            nextBtn.setOnAction {
                GlobalScope.launch {
                    val ipc2 = NvIpc.load(ipcFileName)
                    ipc2.nxt = NXT_LOAD_NEXT
                    ipc2.save(ipcFileName)
                }
            }

            updateStopButtonText(ipc.xit)

            stopBtn.setOnAction {
                GlobalScope.launch {
                    val ipc2 = NvIpc.load(ipcFileName)
                    if (ipc2.xit == XIT_RUNNING) {
                        ipc2.xit = XIT_QUIT
                        Platform.runLater { stopBtn.text = "Start Daemon" }
                    } else {
                        val crp = Paths.get("").toAbsolutePath().toString()
                        Platform.runLater { stopBtn.text = "Stop Daemon" }
                        GlobalScope.launch {
                            ProcessBuilder("$crp/nvdwd.sh").start()
                        }
                    }
                    ipc2.save(ipcFileName)
                }
            }

            val statusCmdVBox = VBox(pauseBtn, nextBtn, stopBtn)
            statusCmdVBox.spacing = 5.0

            val statusHbox = HBox(statusTextVBox, statusCmdVBox)
            statusHbox.spacing = 5.0

            val tgl = ToggleGroup()
            useGnome.isSelected = Settings.desktop == NvSettings.DESKTOP_GNOME
            useGnome.toggleGroup = tgl
            useKde.isSelected = Settings.desktop == NvSettings.DESKTOP_KDE
            useKde.toggleGroup = tgl
            useCustom.isSelected = Settings.desktop == NvSettings.DESKTOP_CUSTOM
            useCustom.toggleGroup = tgl
            tgl.selectedToggleProperty().addListener { _ -> desktopDirty = true }


            customSw.text = Settings.customCmd
            customSw.textProperty().addListener { _ -> customDirty = true }

            dataDir.text = mv.dataPath
            dataDir.textProperty().addListener { _ -> dataDirty = true }



            advPane.isVisible = false

            advBtn.setOnAction {
                if (advBtn.text == "Less") {
                    advBtn.text = "More"
                    advPane.isVisible = false
                } else {
                    advBtn.text = "Less"
                    advPane.isVisible = true
                }
            }
            val vbox = VBox(statusHbox, advBtn, advPane)
            vbox.spacing = 5.0
            vbox.padding = Insets(8.0, 8.0, 8.0, 8.0)
            content = vbox

            startupCheck.setOnAction {
                println("startup: ${startupCheck.isSelected}")

                if(startupCheck.isSelected) {
                    File("/usr/share/applications/nvdwd.desktop")
                        .copyTo(File(desktopEntry))
                } else {
                    File(desktopEntry).delete()
                }
            }

            timer("prefsUpdate", true, 3000L, 1000L) { upd() }
        }
    }

    private fun tsToDateString(ts: Long) : String = fmt.format(ts)

    private fun upd() {
        val ipc = NvIpc.load(ipcFileName)
        mainView?.let { mv ->
            Platform.runLater {
                switchedTime.text = tsToDateString(ipc.lsw)
                chRotate.text = "Last Image Check: ${tsToDateString(ipc.lcr)}"
                currImgName.text = mv.imageName(ipc.cur)
            }

            updateStopButtonText(if((now() - ipc.lip) > LIP_CYCLE_OVERDUE_MILLIS &&
                (ipc.xit == XIT_RUNNING || ipc.xit == XIT_QUIT)) XIT_UNRESPONSIVE else ipc.xit )

            var ssettings = false
            if (ckTimeDirty) {
                Settings.checkTime = ckTime.value
                ssettings = true
                ckTimeDirty = false
            }
            if (swTimeDirty) {
                Settings.switchTime = swTime.value
                ssettings = true
                swTimeDirty = false
            }
            if (desktopDirty) {
                Settings.desktop =
                    when {
                        useGnome.isSelected -> NvSettings.DESKTOP_GNOME
                        useKde.isSelected -> NvSettings.DESKTOP_KDE
                        else -> NvSettings.DESKTOP_CUSTOM
                    }
                ssettings = true
                desktopDirty = false
            }
            if (customDirty) {
                Settings.customCmd = customSw.text
                ssettings = true
                customDirty = false
            }
            if (dataDirty) {
                Settings.dataDir = dataDir.text
                ssettings = true
                dataDirty = false
            }
            if (thumbDirty) {
                Settings.thumbSize = thumbSpin.value
                ssettings = true
                thumbDirty = false
            }
            if (ssettings) {
                GlobalScope.launch {
                    Settings.save()
                    val ipc2 = NvIpc.load(ipcFileName)
                    ipc2.chk = CHK_UPDATE
                    ipc2.save(ipcFileName)
                }
            }
        }
    }

    private fun updateStopButtonText(xit: Byte) {
        Platform.runLater {
            stopBtn.text = when (xit) {
                XIT_QUIT -> WAITING_TEXT
                XIT_TERMINATED -> START_TEXT
                XIT_RUNNING -> STOP_TEXT
                XIT_UNRESPONSIVE -> RESTART_TEXT
                else -> "IPC Error: $xit"
            }
        }
    }

    companion object {
        val fmt = SimpleDateFormat("h:mm:ss a")

        const val START_TEXT = "Start Daemon"
        const val STOP_TEXT = "Stop Daemon"
        const val RESTART_TEXT = "Restart Daemon (unresponsive)"
        const val WAITING_TEXT = "Waiting..."

        const val XIT_UNRESPONSIVE: Byte = 99
    }
}