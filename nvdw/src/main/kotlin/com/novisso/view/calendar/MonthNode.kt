package com.novisso.view.calendar

import javafx.scene.control.Label
import javafx.scene.input.MouseButton
import javafx.scene.layout.GridPane
import javafx.scene.layout.VBox

/**
 * Created by joseph on 3/1/17.
 */
class MonthNode(private val month: Int, internal var calendarNode: CalendarNode) : VBox() {

    //private var month = 0
    private val days = mutableListOf<DayNode>()
    private val gp: GridPane = GridPane()
    private val label: Label

    init {
        //this.month = month
        var r = 0
        var c = 0
        val endDay = DayNode.DaysInMonth[month]
        val doy = DayNode.DaysInMonthSums[month]
        for (i in 0 until endDay) {
            days.add(DayNode(doy + i, this))
            gp.add(days[i], c, r)
            c++
            if (c == 7) {
                c = 0
                r++
            }
        }

        setOnMouseClicked { evt ->
            if(evt.button == MouseButton.PRIMARY){
                if(evt.clickCount == 2) {
                    days.forEach { it.toggleDay(evt) }
                }
            }
        }

        label = Label(MonthNames[month])
        children.addAll(label, gp)
        style = "-fx-border-insets: 0.5mm 0.5mm 0.5mm 0.5mm; -fx-border-color: #76b2f788; " +
                "-fx-border-width: 0.3mm; -fx-border-style: solid;"
    }

    companion object {
        val MonthNames = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
    }

}
