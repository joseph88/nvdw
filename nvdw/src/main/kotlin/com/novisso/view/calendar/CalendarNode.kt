package com.novisso.view.calendar


import com.novisso.model.Img
import javafx.scene.layout.GridPane

/**
 * Created by joseph on 3/1/17.
 */
class CalendarNode(internal var img: Img) : GridPane() {
    private val mnodes = arrayOfNulls<MonthNode>(12)

    init {
        var r = 0
        var c = 0
        for (i in 0..11) {
            mnodes[i] = MonthNode(i, this)
            add(mnodes[i], c, r)
            c++
            if (c == 3) {
                c = 0
                r++
            }
        }
    }
}
