package com.novisso.view.calendar

import com.novisso.mainView
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.input.MouseEvent

/**
 * Created by joseph on 3/1/17.
 */
class DayNode(private val doy: Int, private var monthNode: MonthNode) : Label("${(getMonthDayFromDoy(doy) + 1)}") {
    init {
        stylesheets.add("daynode.css")
        if (monthNode.calendarNode.img.isOnDoy(doy))
            styleClass.add("sel")
        else
            styleClass.add("unsel")

        alignment = Pos.CENTER
        setPrefSize(22.0, 22.0)
        setMinSize(20.0, 20.0)
        setMaxSize(24.0, 24.0)

        setOnMouseClicked { toggleDay(it) }
    }

    fun toggleDay(it: MouseEvent) {
        if (monthNode.calendarNode.img.toggleOnDay(this.doy)) {
            styleClass.clear()
            styleClass.add("sel")
        } else {
            styleClass.clear()
            styleClass.add("unsel")
        }
        //update the index if its available
        mainView?.let {mv ->
            mv.ipdbMgr.imgPackDBIdx.reindexImg(monthNode.calendarNode.img)
        }
    }

    companion object {
        val DaysInMonth = intArrayOf(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
        val DaysInMonthSums = intArrayOf(0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366)

        fun getMonthFromDoy(doy: Int): Int {
            if (doy < 0 || doy > 365) return -1
            val end = DaysInMonthSums.size
            for (i in 0 until end) {
                if (doy < DaysInMonthSums[i]) {
                    return i - 1
                }
            }
            return -1
        }

        fun getMonthDayFromDoy(doy: Int): Int {
            if (doy < 0 || doy > 365) return -1
            val end = DaysInMonthSums.size
            for (i in 0 until end) {
                if (doy < DaysInMonthSums[i + 1]) {
                    return doy - DaysInMonthSums[i]
                }
            }
            return -1
        }

        private const val BASE_STYLE = "-fx-font-size: 10; -fx-border-color: #76b2f733; -fx-border-width: 0.3mm;"
        const val SELECTED_STYLE = "$BASE_STYLE -fx-background-color: #76b2f788; -fx-text-fill: #000000; -fx-font-weight: bold;"
        const val UNSELECTED_STYLE = "$BASE_STYLE -fx-background-color: #000000; -fx-text-fill: #76b2f7;"
    }
}
