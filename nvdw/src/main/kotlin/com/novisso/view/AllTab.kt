package com.novisso.view

import com.novisso.*
import javafx.application.Platform
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield
import kotlin.coroutines.CoroutineContext

/**
 * Created by joseph on 1/4/17.
 */
class AllTab(db: ImgPackDBMgr) : ImageTab("All Images", db), IPDBMgrNotifyTarget, CoroutineScope {
    private val fil: FilteredImgList = FilteredImgList("all")
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx
    private var hasSetInitialVScroll = false

    init {
        imgpane = ImageGridPane(fil, this, Settings.allPos)
        content = imgpane
        scrollVValue = Settings.allPos
        db.notifyMe(this)
    }

    suspend fun forceRefresh(){
        val allImgs = db.imgPackDB.allImages()
        yield()
        val filterImgsList = FilteredImgList("all").addAll(allImgs)
        yield()
        imgpane?.setFilist(filterImgsList)
    }

    override fun updateStatus(status: ImgPackDBMgr.Status) {
        if(status == ImgPackDBMgr.Status.LOADED) {
            launch { forceRefresh() }
        }
    }

    override fun onRefreshComplete() {
        super.onRefreshComplete()
        if(!hasSetInitialVScroll) {
            Platform.runLater {
                imgpane?.vvalueProperty()?.set(Settings.allPos)
            }
            imgpane?.vvalueProperty()?.addListener { _->
                Settings.allPos = imgpane?.vvalue?:0.0
                Settings.save()
            }
        }
    }
}
