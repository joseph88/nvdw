package com.novisso.view

import com.novisso.FilteredImgList
import com.novisso.model.Img
import javafx.application.Platform
import javafx.geometry.Insets
import javafx.scene.control.ScrollPane
import javafx.scene.layout.FlowPane
import kotlinx.coroutines.*
import kotlinx.coroutines.javafx.JavaFx
import java.lang.Exception
import java.util.*
import kotlin.concurrent.schedule
import kotlin.coroutines.CoroutineContext

/**
 * Created by joseph on 12/31/16.
 */
class ImageGridPane(private val owned: ImageTab, protected val startVPos: Double? = null) : ScrollPane(), FilteredImgList.ImgListChangeListener, CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx
    private var filist: FilteredImgList? = null
    private val imgpane: FlowPane = FlowPane()
    private var needsRefresh = false

    init {
        imgpane.hgap = HGAP
        imgpane.vgap = VGAP
        content = imgpane
        padding = Insets(8.0, 8.0, 8.0, 8.0)
        hbarPolicy = ScrollBarPolicy.NEVER
        vbarPolicy = ScrollBarPolicy.AS_NEEDED
        viewportBoundsProperty().addListener { _, _, bounds ->
            imgpane.setPrefSize(bounds.width, bounds.height)
            launch{ drawNodes() }
        }

        vvalueProperty().addListener { _ ->
            launch {
                drawNodes()
            }
        }
        style = "-fx-background-color: #000000;"
        imgpane.style = "-fx-background-color: #000000;"
        startRefresher()
    }

    constructor(list: FilteredImgList?, owned: ImageTab, startVPos: Double? = null) : this(owned, startVPos) {
        setFilist(list)
    }

    fun refresh() {
        //println("[ Refresh Called ${owned.text} ]")
        needsRefresh = true
    }

    private fun startRefresher() {
        //TODO: Do we need GlobalScope.launch here???
        launch {
            while(true) {
                if(needsRefresh) {
                    owned.text = "${owned.text}*"
                    if(startVPos == null) owned.captureVScroll()
                    else owned.scrollVValue = startVPos
                    //println(">>> refresh START: ${owned.text}")
                    imgpane.children.clear()
                    yield()
                    needsRefresh = false
                    filist?.list?.toList()?.let { list ->
                        var yld = 0
                        list.forEach {
                            imgpane.children.add(ImageGridNode(it))
                            if( yld > 30) {
                                yld = 0
                                yield()
                            } else yld++
                        }
                        owned.setCount(list.size)
                    }
                    imgpane.layout()
                    yield()
                    drawNodes()

                    //println(" <<< refresh DONE: ${owned.text}")
                    owned.text = owned.text.removeSuffix("*")
                    owned.onRefreshComplete()
                } else delay(100)
            }
        }
    }

    suspend fun drawNodes() {
        delay(100)
        getVisibleNodes().forEach {
            it.drawImg()
        }
    }

    private fun getVisibleNodes() :  List<ImageGridNode> {
        val l = mutableListOf<ImageGridNode>()
        val bounds = localToScene(boundsInParent)
        if(bounds.width == 0.0 || bounds.height == 0.0) return listOf()
        imgpane.children.forEach {
            val nBounds = it.localToScene(it.boundsInLocal)
            if(bounds.intersects(nBounds)) try{ l.add(it as ImageGridNode) } catch (e: Exception) { }
        }
        return l.toList()
    }

    internal fun setFilist(list: FilteredImgList?) {
        filist?.removeImgListChangeListener(this)
        this.filist = list
        filist?.addImgListChangeListener(this)
        refresh()
    }

    override fun imgListChanged() {
    }

    override fun imgChanged(img: Img) {
        imgpane.children.forEach {
            if(it is ImageGridNode && it.img == img) {
                it.refresh(img)
                return
            }
        }
    }

    companion object {
        private const val HGAP = 4.0
        private const val VGAP = 4.0
    }
}
