package com.novisso.modelrw

import com.beust.klaxon.Klaxon
import com.novisso.model.*
import kotlinx.coroutines.yield
import java.io.File

class ImgPackDBRW : ImgPackDB() {
    fun saveOld(fileName: String) {
        File(fileName).printWriter().use { out ->
            out.print(Klaxon().toJsonString(this))
            out.println()
        }
    }

    suspend fun save(fileName: String) {
        File(fileName).writer().use { out ->
            //magic number
            out.writeInt(MAGIC_NUMBER)
            //file version
            out.writeShort(fileVer)
            //next ID
            out.writeInt(nextId)
            //pack count
            out.writeShort(packs.size)
            //packs
            packs.forEach { pack ->
                pack.write(out)
                yield()
            }
        }
    }

    fun save2(fileName: String) {
        File(fileName).writer().use { out ->
            //magic number
            out.write("$MAGIC_NUMBER|")
            //file version
            out.write("$fileVer|")
            //next ID
            out.write("$nextId|")
            //pack count
            out.write("${packs.size}|")
            //packs
            packs.forEach { pack ->
                pack.write2(out)
            }
        }
    }

    fun removeImage(img: Img, imagePath: String, thumbPath: String) {
        packs.forEach {
            val fimg = it.imgs.find { i -> i.uuid == img.uuid }
            if(fimg != null){
                it.imgs.remove(fimg)
                File("$imagePath/${fimg.id}${Img.EXT}").delete()
                File("$thumbPath/${fimg.id}${Img.EXT}").delete()
                idx?.removeImg(fimg)
                return
            }
        }
    }

    fun removePack(pack: ImgPack, imagePath: String, thumbPath: String) {
        pack.imgs.forEach {
            File("$imagePath/${it.id}${Img.EXT}").delete()
            File("$thumbPath/${it.id}${Img.EXT}").delete()
            idx?.removeImg(it)
        }
        pack.imgs.clear()
        packs.remove(pack)
    }

    companion object {
        fun load(fileName: String) : ImgPackDBRW {
            val ipdb = ImgPackDBRW()
            ipdb.loadFill(fileName)
            return ipdb
        }
    }
}