plugins {
    kotlin("jvm") version "1.3.72"
    id("org.openjfx.javafxplugin") version "0.0.8" apply true
}

buildscript {
    repositories {
        maven {
            setUrl("https://plugins.gradle.org/m2/")
        }
    }
    dependencies {
        classpath("org.openjfx:javafx-plugin:0.0.8")
    }
}

group = "com.novisso"
version = "7.1"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.openjfx:javafx:14")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.5")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.3.5")
    implementation("com.beust:klaxon:5.0.12")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.3.72")
}

javafx {
    version = "14"
    modules("javafx.controls")
    //modules("javafx.controls", "javafx.media", "javafx.graphics", "javafx.fxml")
}

val fatJar = task("fatJar", type = Jar::class) {
    manifest {
        attributes["Implementation-Title"] = "nvdw"
        attributes["Implementation-Version"] = archiveVersion
        attributes["Main-Class"] = "com.novisso.NvdwAppKt"
    }
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    with(tasks.jar.get() as CopySpec)
}

tasks {

    "build" {
        dependsOn(fatJar)
    }

    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}
