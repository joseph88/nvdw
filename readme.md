Novisso Desktop Wallpaper Rotator

This project will rotate the wallpaper on Gnome Desktop 3.  It is written in Kotlin using JavaFX.  The gimmick with this rotator is that each image can be give a specific set of dates to show so you are getting new, seasonal images all the time.  In my personal collection I have over 2000 images, but only about 8 or so that rotate each day.  So i get spring images in the spring time, summer images in summer etc.

To add an image go to the channel tab, create a channel by giving it a name then drag and drop
images onto the channel page.  Click on an image to edit its name, change the dates it will be show or delete it.

There are launch scripts included.  There are two parts to this application

The nvdwd.sh script will execute the daemon that does the switching and rotation.  The
nvdw.sh script will execute the manager program.

In theory this application could be used with other desktops but I have only ever tested it
with Gnome 3 and Mate.

To install the application put the contents of the linux_run directory in /opt/nvdw.  If desired copy the nvdw.desktop file to your distrobution's applications directory and you will get a menu entry for the manger.  I also copy the nvdwd.desktop file to my .autostart directory so the application starts when I login.

To build issue a 

```./gradlew clean build```

command in the nvdw and nvdwd directories, this will produce a fat jar. 


For windows refer to:

https://github.com/philhansen/WallpaperChanger/releases/


Debreate is used to create the Debian .deb install package
