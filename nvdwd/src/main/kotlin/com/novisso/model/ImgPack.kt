package com.novisso.model

import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.lang.StringBuilder
import java.nio.file.Paths
import java.util.*

class ImgPack(
    var name: String = "",
    var descr: String? = "",
    val imgs: MutableList<Img> = mutableListOf(),
    val uuid: String = "${UUID.randomUUID()}"
) {
    fun createNewImage(srcPath: String, id: Int) : Img {
        val p = Paths.get(srcPath)
        var name = p.fileName.toString()
        val i = name.indexOf('.')
        if (i > 0) name = name.substring(0, i)
        if(name.length > 20) name = name.substring(0,20)
        val img = Img(name, srcPath, id)
        imgs.add(img)
        return img
    }

    fun write(out: OutputStreamWriter) {
        out.writeString(name)
        out.writeString(descr?:"")
        out.writeString(uuid)
        out.writeShort(imgs.size)
        imgs.forEach { img ->
            img.write(out)
        }
    }

    override fun toString(): String = name

    companion object {
        fun read(inp: InputStreamReader) : ImgPack {
            val name = inp.readString()
            val descr = inp.readString()
            val uuid = inp.readString()
            val sz = inp.readShort()
            if(sz < 0) throw Exception("Invalid Img Count $sz")
            val imgs = mutableListOf<Img>()
            for(x in 1..sz) {
                imgs.add(Img.read(inp))
            }
            return ImgPack(name, descr, imgs, uuid)
        }
    }

    //DEBUG toString Version
    fun debugString(): String {
        val sb = StringBuilder(" $name : $descr : $uuid \n-----------------------------------------\n")
        imgs.forEach { sb.append(it.toString()); sb.append("\n")}
        return sb.toString()
    }
}
