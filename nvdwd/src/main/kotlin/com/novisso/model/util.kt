package com.novisso.model

import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.time.LocalDate
import java.util.*

const val IMG_DIR = "imgs"
const val WALL_DIR = "wallpaper"
const val THUMB_DIR = "thumbs"

fun getTodayDoy(): Int = Date().getDoy()

fun getDoyFromDate(d: Date): Int {
    val c = Calendar.getInstance()
    c.time = d
    val doy = c.get(Calendar.DAY_OF_YEAR)-1
    return if(LocalDate.now().isLeapYear || doy < 59) doy else doy+1
}

fun Date.getDoy(): Int = getDoyFromDate(this)

fun now(millis: Long = 0L): Long {
    if(millis == 0L) return Date().time
    val gc = GregorianCalendar(TimeZone.getTimeZone("UTC"))
    gc.add(Calendar.MILLISECOND, millis.toInt())
    return gc.timeInMillis
}


fun InputStreamReader.readByte(): Byte = read().toByte()
fun InputStreamReader.readInt(): Int {
    var i = read()
    i = i.or(read().shl(Byte.SIZE_BITS))
    i = i.or(read().shl(Byte.SIZE_BITS * 2))
    i = i.or(read().shl(Byte.SIZE_BITS*3))
    return i
}
fun InputStreamReader.readShort(): Int {
    var i = read()
    i = i.or(read().shl(Byte.SIZE_BITS))
    return i
}
fun InputStreamReader.readLong(): Long {
    var i = read().toLong()
    i = i.or(read().toLong().shl(Byte.SIZE_BITS))
    i = i.or(read().toLong().shl(Byte.SIZE_BITS * 2))
    i = i.or(read().toLong().shl(Byte.SIZE_BITS*3))
    i = i.or(read().toLong().shl(Byte.SIZE_BITS*4))
    i = i.or(read().toLong().shl(Byte.SIZE_BITS*5))
    i = i.or(read().toLong().shl(Byte.SIZE_BITS*6))
    i = i.or(read().toLong().shl(Byte.SIZE_BITS*7))
    return i
}
fun InputStreamReader.readString(): String = buildString {
        var c = read()
        while(c != 0){
            append(c.toChar())
            c = read()
        }
    }

fun OutputStreamWriter.writeByte(b: Byte) = write(b.toInt())
fun OutputStreamWriter.writeInt(value: Int) {
    var i = value
    for(x in 1..Int.SIZE_BYTES){
        write(i)
        i = i.shr(Byte.SIZE_BITS)
    }
}
fun OutputStreamWriter.writeShort(value: Int) {
    var i = value
    for(x in 1..2){
        write(i)
        i = i.shr(Byte.SIZE_BITS)
    }
}
fun OutputStreamWriter.writeLong(value: Long) {
    var i = value
    for(x in 1..Long.SIZE_BYTES){
        write(i.toInt())
        i = i.shr(Byte.SIZE_BITS)
    }
}
fun OutputStreamWriter.writeString(value: String) = write("$value\u0000")

