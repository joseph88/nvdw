package com.novisso.model

import java.io.File
import java.lang.Exception
import java.lang.StringBuilder

class ImgPackDBIdx {
    val days: Array<MutableSet<Int>> = Array(366) { mutableSetOf<Int>() }
    fun getTodayImgIds(doy: Int) : List<Int> = days[doy].toList()

    fun removeImg(img: Img) {
        img.dates.forEach { days[it].remove(img.id) }
    }

    fun reindexImg(img: Img) {
        days.forEachIndexed { idx, set ->
            if(img.dates.contains(idx)) set.add(img.id)
            else set.remove(img.id)
        }
    }

    fun save(fileName: String) {
        File(fileName).writer().use { out ->
            days.forEachIndexed { idx, set ->
                out.writeShort(idx)
                out.writeShort(set.size)
                set.forEach { imgId ->
                    out.writeInt(imgId)
                }
            }
        }
    }

    fun debugString(): String {
        val sb = StringBuilder("Image Pack Idx \n==============================================\n")
        days.forEachIndexed { idx, lst ->
            sb.append('[').append(idx).append("] ")
            lst.forEach { sb.append(it).append(", ") }
            sb.append("\n---\n")
        }
        return sb.toString()
    }

    companion object {
        const val IDX_FILE_NAME = "idx.bin"

        fun load(fileName: String) : ImgPackDBIdx {
            val ipdb = ImgPackDBIdx()
            try {
                val f = File(fileName)
                var idx : Int
                var size : Int
                var imgId: Int
                f.reader().use { inp ->
                    for (i in 0 until 366) {
                        idx = inp.readShort()
                        if (idx == i) {
                            size = inp.readShort()
                            if(size < 0) throw Exception("index row size cannot be negative $size")
                            if(size > 32_767) throw Exception("index row size cannot exceed 32,767. $size")
                            for(cnt in 0 until size) {
                                imgId = inp.readInt()
                                if(imgId < 0) throw Exception("invalid Image Id $imgId")
                                ipdb.days[idx].add(imgId)
                            }
                        } else {
                            throw Exception("index key mismatch key $idx from file != $i from input routine.")
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return ipdb
        }
    }
}
