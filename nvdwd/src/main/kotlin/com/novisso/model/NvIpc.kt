package com.novisso.model

import java.io.*

/**
 *  xit = 1 to exit, 0 is running, 2 is exited
 *  chk = 1 to force settings update and force a check and rotate
 *  nxt = 1 to load next wallpaper
 *  paz = 1 to pause, 0 to unpause
 *  lim = [image id] to load a specific image from the current set
 *  all = [image id] to load a specific image from the entire set
 *  cur = [curr img id]
 *  lcr = [last check and rotate time]
 *  lsw = [last wallpaper switch time]
 *  lip = [last ipc czech time - use to test for up-ness]
 */
class NvIpc(
    var xit: Byte = XIT_TERMINATED,
    var chk: Byte = CHK_WAITING,
    var nxt: Byte = NXT_WAITING,
    var paz: Byte = PAZ_CONTINUE,
    var lim: Int = LIM_WAITING,
    var all: Int = ALL_WAITING,
    var cur: Int = CUR_UNSET,
    var lcr: Long = LCR_UNSET,
    var lsw: Long = LSW_UNSET,
    var lip: Long = LIP_UNCHECKED
) {
    fun save(fileName: String) {
        File(fileName).writer().use { out ->
            out.writeByte(xit)
            out.writeByte(chk)
            out.writeByte(nxt)
            out.writeByte(paz)
            out.writeInt(lim)
            out.writeInt(all)
            out.writeInt(cur)
            out.writeLong(lcr)
            out.writeLong(lsw)
            out.writeLong(lip)
        }
    }

    companion object {
        fun load(fileName: String) : NvIpc {
            val f = File(fileName)
            val ipc = NvIpc()
            if(f.exists()){
                f.reader().use { ip ->
                    ipc.xit = ip.readByte()
                    ipc.chk = ip.readByte()
                    ipc.nxt = ip.readByte()
                    ipc.paz = ip.readByte()
                    ipc.lim = ip.readInt()
                    ipc.all = ip.readInt()
                    ipc.cur = ip.readInt()
                    ipc.lcr = ip.readLong()
                    ipc.lsw = ip.readLong()
                    ipc.lip = ip.readLong()
                }
            }
            return ipc
        }

        const val IPC_FILE_NAME = "ipc.bin"

        const val XIT_QUIT : Byte = 1
        const val XIT_RUNNING : Byte = 0
        const val XIT_TERMINATED : Byte = 2
        const val CHK_UPDATE : Byte = 1
        const val CHK_WAITING : Byte = 0
        const val NXT_LOAD_NEXT : Byte =1
        const val NXT_WAITING : Byte = 0
        const val PAZ_PAUSE : Byte = 1
        const val PAZ_CONTINUE : Byte = 0
        const val LIM_WAITING = 0
        const val ALL_WAITING = 0
        const val CUR_UNSET = 0
        const val LCR_UNSET = 0L
        const val LSW_UNSET = 0L
        const val LIP_UNCHECKED = 0L
        const val LIP_CYCLE_SECS = 20
        const val LIP_CYCLE_OVERDUE_MILLIS = LIP_CYCLE_SECS *1200
    }

    override fun toString(): String = "xit=$xit, chk=$chk, nxt=$nxt, paz=$paz, lim=$lim, all=$all, cur=$cur, lcr=$lcr, lsw=$lsw, lip=$lip"
}
