package com.novisso

import com.novisso.model.*
import com.novisso.model.ImgPackDBIdx.Companion.IDX_FILE_NAME
import com.novisso.model.NvIpc.Companion.ALL_WAITING
import com.novisso.model.NvIpc.Companion.CHK_UPDATE
import com.novisso.model.NvIpc.Companion.CHK_WAITING
import com.novisso.model.NvIpc.Companion.LIM_WAITING
import com.novisso.model.NvIpc.Companion.LIP_CYCLE_SECS
import com.novisso.model.NvIpc.Companion.NXT_LOAD_NEXT
import com.novisso.model.NvIpc.Companion.NXT_WAITING
import com.novisso.model.NvIpc.Companion.PAZ_CONTINUE
import com.novisso.model.NvIpc.Companion.XIT_QUIT
import com.novisso.model.NvIpc.Companion.XIT_RUNNING
import com.novisso.model.NvIpc.Companion.XIT_TERMINATED
import java.io.File
import java.io.IOException
import java.lang.Thread.sleep
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

const val TICK_LEN = 100L
val TICKS_PER_S = (1000/TICK_LEN).toInt()

var settings = NvSettings()
var ipc = NvIpc()
val cfgDir = "${System.getProperty("user.home")}/.config/nvdw"
val cfgFileName = "$cfgDir/config.json"
val ipcFileName = "$cfgDir/ipc.bin"
var currImgs = listOf<Int>()
var currIdx = 0

fun main(args: Array<String>) {
    //make sure the cfgDir is valid
    File(cfgDir).mkdirs()
    loadSettings()
    ipc = NvIpc.load(ipcFileName)
    ipc.xit = XIT_RUNNING
    ipc.save(ipcFileName)
    println(ipc)

    checkAndRotate()

    var ipcTicks = 0L
    var swTicks = 0
    var chTicks = settings.checkTime * TICKS_PER_S
    var lipTicks = 0
    do {
        ipcTicks--
        if(ipcTicks < 1){
            ipcTicks = settings.ipcTimeMs / TICK_LEN
            ipc = NvIpc.load(ipcFileName)
            if(ipc.chk == CHK_UPDATE) {
                ipc.chk = CHK_WAITING
                ipc.save(ipcFileName)
                loadSettings()
                ipcTicks = settings.ipcTimeMs / TICK_LEN
                swTicks = settings.switchTime * TICKS_PER_S
                chTicks = settings.checkTime * TICKS_PER_S
                checkAndRotate()
            }
            when {
                ipc.nxt == NXT_LOAD_NEXT -> {
                    ipc.nxt = NXT_WAITING
                    ipc.save(ipcFileName)
                    switchImage()
                }
                ipc.lim != LIM_WAITING -> {
                    val id = ipc.lim
                    ipc.lim = LIM_WAITING
                    ipc.save(ipcFileName)
                    switchTo(id)
                }
                ipc.all != ALL_WAITING -> {
                    val id = ipc.all
                    ipc.all = ALL_WAITING
                    ipc.save(ipcFileName)
                    switchTo(id)
                }
            }
        }
        lipTicks--
        if(lipTicks < 1) {
            lipTicks = LIP_CYCLE_SECS * TICKS_PER_S
            ipc.lip = now()
            ipc.save(ipcFileName)
        }
        swTicks--
        if(swTicks < 1){
            swTicks = settings.switchTime * TICKS_PER_S
            //println("switch $swTicks")
            if(ipc.paz == PAZ_CONTINUE) switchImage()
        }
        chTicks--
        if(chTicks < 1){
            chTicks = settings.checkTime * TICKS_PER_S
            //println("check $chTicks")
            checkAndRotate()
        }
        sleep(TICK_LEN)
    } while(ipc.xit != XIT_QUIT)
    ipc.xit = XIT_TERMINATED
    ipc.save(ipcFileName)
}

//fun initTest() {
    //val img = Img("test1", "test1.png", mutableListOf(2, 3, 4))
    //val ip = ImgPack("testPk1", "testpack", mutableListOf(img))
    //val ipdb = ImgPackDB(mutableMapOf(ip.uuid to ip))
    //ipdb.save(settings.dataPathResolved(cfgDir)+"/db.json", ipdb)

    //val ipdbr = ImgPackDB.load(settings.dataPathResolved(cfgDir)+"/db.json")
    //println(ipdbr.getDayImages(getTodayDoy()))
//}

fun dataPath() = settings.dataPathResolved(cfgDir)

fun loadSettings() {
    settings = NvSettings.load(cfgFileName)
    File(dataPath()).mkdirs()
    File("${dataPath()}/$WALL_DIR").mkdirs()
    File("${dataPath()}/$IMG_DIR").mkdirs()
}

// chcek for updates and rotate the images.
fun checkAndRotate() {
    currImgs = ImgPackDBIdx.load("${dataPath()}/$IDX_FILE_NAME").getTodayImgIds(getTodayDoy())
    if(currIdx >= currImgs.size) currIdx = 0
    val walldir = "${dataPath()}/$WALL_DIR"
    val f = File(walldir)
    f.mkdirs()

    val tlst = currImgs
    val flst = f.listFiles()?.map {
        try{
            it.name.substring(0, it.name.length - Img.EXT.length).toInt()
        }catch (e: Exception) {
            println("${it.name} :: ${e.message}")
        }
    }

    flst?.forEach { fid ->
        //println("checking for $fid in $tlst")
        if (!tlst.contains(fid)) {
            File("$walldir/$fid${Img.EXT}").delete()
            //println("not there so nukeing ${walldir + fid + Img.EXT}")
            //dm.delete()
        }
    }
    for (fid in tlst) {
        if (flst?.contains(fid) == false) {
            val i = currImgs.find { it == fid }
            if (i != null) {
                try {
                    Files.copy(
                        Paths.get("${dataPath()}/$IMG_DIR/$i${Img.EXT}"),
                        Paths.get("$walldir/$fid${Img.EXT}"),
                        StandardCopyOption.REPLACE_EXISTING
                    )
                } catch (ioe: Exception) {
                    ioe.printStackTrace()
                }
            }
        }
    }
    ipc.lcr = now()
    ipc.save(ipcFileName)
}

fun switchImage(){
    val img = currImgs[currIdx++]
    if(currIdx >= currImgs.size) currIdx = 0
    switchTo(img)
    ipc.lsw = now()
    ipc.cur = img
    ipc.save(ipcFileName)
}

//fun switchTo(id: Int) {
//    val img = currImgs.find { id == it }
//    if(img != null) switchTo(img)
//}

/*fun switchToFromAll(id: Int) {
    if(!uuid.isBlank()) {
        val allImgs = ImgPackDB.load("${dataPath()}/db.json").allImages()
        val img = allImgs.find { uuid == it.uuid }
        if (img != null) switchTo(img)
    }
}*/

fun switchTo(id: Int) {
    //find image...
    val imgPath = "${dataPath()}/$IMG_DIR/$id${Img.EXT}"
    //println("switching to $imgPath")
    when(settings.desktop) {
        NvSettings.DESKTOP_GNOME -> switchGnome(imgPath)
        NvSettings.DESKTOP_KDE -> switchPlasma(imgPath)
        NvSettings.DESKTOP_CUSTOM -> switchCustom(imgPath)
    }
}


fun switchGnome(imgPath: String) {
    //desktop
    val pb = ProcessBuilder("gsettings", "set", "org.gnome.desktop.background", "picture-uri", imgPath)
    try {
        pb.start()
    } catch (ioe: IOException) {
        ioe.printStackTrace()
    }
    //lock screen
    val pbl = ProcessBuilder("gsettings", "set", "org.gnome.desktop.screensaver", "picture-uri", imgPath)
    try {
        pbl.start()
    } catch (ioe: IOException) {
        ioe.printStackTrace()
    }
}

fun switchPlasma(imgPath: String) {
    //desktop
    val pb = ProcessBuilder("qdbus", "org.kde.plasmashell", "/PlasmaShell", "org.kde.plasmaShell.evaluateScript", kdeTemplate(imgPath))
    try {
        pb.start()
    } catch (ioe: IOException) {
        ioe.printStackTrace()
    }
    //lock screen
}

fun switchCustom(imgPath: String) {
    val cms = settings.customCmd.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val cml = arrayOfNulls<String>(cms.size + 1)
    for (x in cms.indices) cml[x] = cms[x]
    cml[cms.size] = imgPath

    if (cml.isEmpty()) return
    val pb = ProcessBuilder(*cml)
    try {
        //Process p = pb.start();
        pb.start()
    } catch (ioe: IOException) {
        ioe.printStackTrace()
    }
}

fun kdeTemplate(imgFilePath: String) = """'var allDesktops = desktops();print (allDesktops);for (i=0;i<allDesktops.length;i++){d = allDesktops[i];d.wallpaperPlugin="org.kde.image";d.currentConfigGroup = Array("Wallpaper", "org.kde.image", "General"); d.writeConfig("Image", "file://$imgFilePath");}'"""